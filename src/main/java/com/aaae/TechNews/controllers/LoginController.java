package com.aaae.TechNews.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aaae.TechNews.domains.User;
import com.aaae.TechNews.repositories.UserRepository;
import com.aaae.TechNews.security.LoginForm;
@Controller
@RequestMapping("/login")
public class LoginController {

	
	 private UserRepository userRepo;
	 @GetMapping()
	public String loginForm() {
		return "/login";
	}
	
	@PostMapping()
	public String processLogin(LoginForm form) {
		 String username = form.getUsername();
		 User user = userRepo.findByUsername(username);
		 if (user.getPassword() == form.getPassword()) {
			 return "/";
	}
		 else {
			 return "/login";
		 }
		 }
		 
}
