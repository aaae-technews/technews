package com.aaae.TechNews.controllers;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aaae.TechNews.repositories.UserRepository;
import com.aaae.TechNews.security.RegistrationForm;

@Controller
@RequestMapping("/Register")
public class RegistrationController {

	 private UserRepository userRepo;
	 private PasswordEncoder passwordEncoder;
	 private RegistrationForm form;
	 
	  public RegistrationController(
		      UserRepository userRepo, PasswordEncoder passwordEncoder) {
		    this.userRepo = userRepo;
		    this.passwordEncoder = passwordEncoder;
		  }
	  
	 @GetMapping
	  public String registerForm() {
	    return "Register";
	  }
	  
	  @PostMapping
	  public String processRegistration(RegistrationForm form) {
	    this.form = form;
		userRepo.save(form.toUser(passwordEncoder));
	    return "/login";
	  }
}
