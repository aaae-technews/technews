package com.aaae.TechNews.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aaae.TechNews.repositories.SubscriberRepository;
import com.aaae.TechNews.security.SubscriptionForm;

@Controller
@RequestMapping("/subscribe")
public class SubscriptionController {
	 private SubscriberRepository subscriberRepo;
	 public SubscriptionController(
			SubscriberRepository subscriberRepo) {
		    this.subscriberRepo = subscriberRepo;
		  }
	  
	 @GetMapping
	  public String subscriptionForm() {
	    return "/subscribe";
	  }
	  
	  @PostMapping
	  public String processRegistration(SubscriptionForm form) {
	    subscriberRepo.save(form.toSubscriber());
	    return "/";
	  }

}
