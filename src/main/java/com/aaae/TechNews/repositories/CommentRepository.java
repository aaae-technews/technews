package com.aaae.TechNews.repositories;
import org.springframework.data.repository.CrudRepository;
import com.aaae.TechNews.domains.Comment;

public interface CommentRepository 
		extends CrudRepository<Comment, Long>{

}
