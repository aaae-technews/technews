package com.aaae.TechNews.repositories;
import org.springframework.data.repository.CrudRepository;
import com.aaae.TechNews.domains.Admin;
public interface AdminRepository
		extends CrudRepository<Admin, Long>{

}
