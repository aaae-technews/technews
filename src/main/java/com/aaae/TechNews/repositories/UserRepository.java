package com.aaae.TechNews.repositories;
import org.springframework.data.repository.CrudRepository;
import com.aaae.TechNews.domains.User;
public interface UserRepository 
		extends CrudRepository<User, Long>{
	
	User findByUsername(String username);

}
