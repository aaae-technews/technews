package com.aaae.TechNews.repositories;
import org.springframework.data.repository.CrudRepository;
import com.aaae.TechNews.domains.Category;
interface CategoryRepository 
		extends CrudRepository<Category, Long>{

}
