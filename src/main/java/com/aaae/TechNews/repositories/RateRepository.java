package com.aaae.TechNews.repositories;
import org.springframework.data.repository.CrudRepository;
import com.aaae.TechNews.domains.Rate;
public interface RateRepository 
		extends  CrudRepository<Rate, Long>{
		
}
