package com.aaae.TechNews.repositories;
import org.springframework.data.repository.CrudRepository;
import com.aaae.TechNews.domains.Subscriber;
public interface SubscriberRepository 
		extends CrudRepository<Subscriber, Long>{

}
