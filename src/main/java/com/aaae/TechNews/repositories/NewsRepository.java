package com.aaae.TechNews.repositories;
import com.aaae.TechNews.domains.News;

import java.awt.print.Pageable;
import java.util.*;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface NewsRepository
		extends CrudRepository<News, Long>{
	

}
