package com.aaae.TechNews.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access=AccessLevel.PRIVATE, force=true)
@RequiredArgsConstructor
@Table(name="comment")
public class Comment {
	
	@Id
	private final Long newsId;
	
	@NotBlank(message="comment body cannot be empty")
	private String content;
	
	private Long userId;

}
