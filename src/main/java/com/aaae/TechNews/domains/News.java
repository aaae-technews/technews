package com.aaae.TechNews.domains;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;


@Data
@Entity
@NoArgsConstructor(access=AccessLevel.PRIVATE, force=true)
@RequiredArgsConstructor
@Table(name="news")
public class News {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private final Long id;
	
	
	
	@NotBlank(message="Title is required")
	private String title;
	
	@NotBlank(message="author is required")
	private String author;
	
	@NotBlank(message="imageUrl is required")
	private String imageUrl;
	
	@NotBlank(message="content is required")
	private String content;
	
	private Date publishedAt;
	@PrePersist
	void publishedAt() {
		this.publishedAt = new Date();
	}
	
}
