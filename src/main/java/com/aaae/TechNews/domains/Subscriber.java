package com.aaae.TechNews.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access=AccessLevel.PRIVATE, force=true)
@RequiredArgsConstructor
@Table(name="subscriber")
public class Subscriber {
	
	@Id
	private final Long id;
	
	@NotBlank(message="name cannot be empty")
	private final String name;
	
	@NotBlank(message="email is required")
	private final String email;

}
