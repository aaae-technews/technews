package com.aaae.TechNews.security;

import java.util.Random;

import com.aaae.TechNews.domains.Subscriber;

import lombok.Data;

@Data
public class SubscriptionForm {
	private String name;
	private String email;
	
	Random random = new Random();
	Long id =  random.nextLong();
	public Subscriber toSubscriber() {
		return new Subscriber(id, name, email);
	}

}
