package com.aaae.TechNews.security;

import java.util.Random;

import org.springframework.security.crypto.password.PasswordEncoder;
import com.aaae.TechNews.domains.User;
import lombok.Data;

@Data
public class RegistrationForm {
	private String fullname;
	private String password;
	private String username;
	private String email;
	Random random = new Random();
	Long id =  random.nextLong();
	
	
	 public User toUser(PasswordEncoder passwordEncoder) {
		    return new User(id, fullname, username, passwordEncoder.encode(password), email);
		  }

}
