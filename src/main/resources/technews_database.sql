

CREATE DATABASE IF NOT EXISTS `technews_database`
;


CREATE TABLE IF NOT EXISTS `admin` (
  `userName` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
);

 


CREATE TABLE IF NOT EXISTS `comment` (
  `userID` int(25) NOT NULL,
  `newsID` int(25) NOT NULL,
  `content` text NOT NULL
);


CREATE TABLE IF NOT EXISTS `news` (
`newsID` int(25) NOT NULL,
  `date` date NOT NULL,
  `title` text NOT NULL,
  `imageUrl` varchar(100) NOT NULL,
  `content` text NOT NULL
) ;


CREATE TABLE IF NOT EXISTS `rate` (
  `newsID` int(25) NOT NULL,
  `userID` int(25) NOT NULL
);


CREATE TABLE IF NOT EXISTS `subscribe` (
`subID` int(25) NOT NULL,
  `userName` varchar(25) NOT NULL,
`email` varchar(50) NOT NULL
);



CREATE TABLE IF NOT EXISTS `user` (
  `userID` int(25) NOT NULL,
  `userName` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `email` varchar(40) NOT NULL
);



ALTER TABLE `comment`
 ADD KEY `userID` (`userID`), ADD KEY `newsID` (`newsID`);


ALTER TABLE `news`
 ADD PRIMARY KEY (`newsID`);

ALTER TABLE `rate`
 ADD KEY `newsID` (`newsID`), ADD KEY `newsID_2` (`newsID`), ADD KEY `userID` (`userID`);

ALTER TABLE `subscribe`
 ADD PRIMARY KEY (`subID`);



ALTER TABLE `user`
 ADD PRIMARY KEY (`userID`);
ALTER TABLE `news`
MODIFY `newsID` int(25) NOT NULL AUTO_INCREMENT;


ALTER TABLE `subscribe`
MODIFY `subID` int(25) NOT NULL AUTO_INCREMENT;

ALTER TABLE `comment`
ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`newsID`) REFERENCES `news` (`newsID`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE `rate`
ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`newsID`) REFERENCES `news` (`newsID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;
